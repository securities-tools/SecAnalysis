/**
 * The act of obtaining EOD data is constrained by the fact that we are getting limited volumes of free data.
 *
 * 1. If we have downloaded a company this month, keep downloading it for the rest of the month.
 * 2. If necessary, download as much historic data as relevant
 * 3. If we have any room for in our budget (500 companies per month), identify which companies
 *    are our best candidates, and add them.
 */

import fetch from 'node-fetch';
import * as Streams from 'stream';

import * as utils from './lib/utils.js';
import * as global from './global.js';
import * as deposit from './lib/deposit.js';
import {
	GetObjectCommand,
	HeadObjectCommand,
} from "@aws-sdk/client-s3";


const maxParallelProc = 1;
const APIKEY = global.settings.prices.tiingo_key;
const tiingoLimit = global.settings.prices.limit;


async function* fetchEOD(){
	// Retrieve this month's current usage
	let compList = await getCompanyList();
	for await (let comp of compList){
		comp = await getDownloadParameters(comp);
		if(!comp)continue;
		comp = await DownloadPrices(comp);
		if(!comp)continue;
		comp = await BuildFactPackage(comp);
		if(!comp)continue;
		yield comp;
	}
	return;
}

async function main(){
	// Run it all
	let downloads = [];
	for await(let item of fetchEOD()){
		item.source = 'tiingo';
		let total = deposit.ReceiveData(item,global.settings)
			.then(d=>{
				total.isResolved = true;
				return d;
			})
			.catch((e)=>{
				//debugger;
				console.error(e.message);
				total.isResolved = true;
				return null;
			})
			.then(d=>{
				return d;
			});

		downloads.push(total);
		if(downloads.length >= maxParallelProc){
			try{
				await Promise.race(downloads);
			}
			catch(e){
				if (e.message === 'All Promises rejected'){
					downloads = [];
				}
			}
			for(let i = 0; i< downloads.length; i++){
				if(downloads[i].isResolved){
					downloads[i] = downloads.at(-1);
					downloads.pop();
					i--;
				}
			}
		}
	}
	await Promise.all(downloads);
	console.log('Done.');
	process.exit();
}


let _getAllSymbols = null;
async function getAllSymbols(){
	if (_getAllSymbols) return _getAllSymbols;

	let symbols = global.sec.send(new GetObjectCommand({Bucket: 'facts', Key: 'views/symbols'}));
	symbols = await symbols;

	let rs = {};
	try{
		rs = symbols;
		let buffer = [];
		for await (let chunk of rs.Body) {
			chunk = Array.from(chunk);
			buffer = buffer.concat(chunk);
		}
		buffer = Buffer.from(buffer);
		rs = buffer.toString()
		rs = JSON.parse(rs);
	}
	catch(e){
		if(e.Code == 'NoSuchKey'){
			// that's fine.... we will be creating one
			console.log('No previous prices downloaded.')
			rs = {
				'MMM':{
					market: 'NYSE',
					symbol: 'MMM',
					date: '2023-01-01',
					cik: 66740,
				}
			};
		}
		else{
			throw e;
		}
	}

	_getAllSymbols =  rs;
	return _getAllSymbols;

}


let _getPrevDownloads = null;
async function getPrevDownloads(key){
	if(_getPrevDownloads) return _getPrevDownloads;

	let rs = {};
	try{
		rs = await global.sec.send(new GetObjectCommand({Bucket: 'facts', Key: 'views/price_dloads'}));
		rs = rs.Body.read().toString('utf8');
		rs = JSON.parse(rs);
	}
	catch(e){
		if(e.Code == 'NoSuchKey'){
			// that's fine.... we will be creating one
			console.log('No previous prices downloaded.')
			rs = {
				"202301": ['MMM']
			};
		}
		else{
			debugger;
			throw e;
		}
	}
	let tickers = new Map();
	(rs[key] ?? [])
		.map((r)=>{
			return [r.key[2].toUpperCase(),r.key[3]];
		})
		.forEach(d=>{
			tickers.set(...d);
		})
		;

	_getPrevDownloads = tickers;
	return _getPrevDownloads;
}


/**
 * Retrieve the list of companies that we should download.
 */
async function getCompanyList(){
	// get the current date
	let key = new Date();
	key = [
		key.getUTCFullYear(),
		key.getUTCMonth()+1
	];
	// fetch all the companies we have already done downloads
	// for this month. Because we are constrained, we should
	// download the daily data for this.

	let tickers = await getPrevDownloads(key);
	let allSymbols = await getAllSymbols();

	// if we have not downloaded enough tickers to fill our
	// quota, we should add some more to our downloader.
	if(tickers.size < tiingoLimit){

		for(let ticker of global.settings.prices.favoured){
			ticker = ticker.toUpperCase();
			//tickers.set(ticker,null);
			if(ticker in allSymbols){
				let cik = allSymbols[ticker].cik;
				tickers.set(ticker,cik);
			}
			if(tickers.size >= tiingoLimit) break;
		}
	}


	// if we have still not filled our
	// quota, we should add some more to our downloader.
	let allsymb = Object.entries(allSymbols).map(d=>{
		return [d[0],d[1].cik];
	});
	let breaker = tiingoLimit*2;
	while(tickers.size < tiingoLimit){
		let r = Math.floor(Math.random()*allsymb.length);
		let s = allsymb[r];
		tickers.set(...s)
		if(--breaker<0){
			throw new Error('Insufficient Symbols causing infinite loop');
		}
	}

	let stm = Streams.Readable.from(tickers.entries(),{objectMode:true});

	return stm;
}


let _today = null;
function Today(){
	if(_today) return _today;

	let today = new Date();
	today = Date.UTC(today.getFullYear(),today.getMonth(), today.getDate());
	today = new Date(today);
	today = today.toISOString().split('T')[0];
	today = today.split('-'); today[1]--;
	today = Date.UTC(...today);

	_today = today;
	return _today;
}


let _epoch = null;
function Epoch(){
	if (_epoch) return _epoch;

	let epoch = '1994-12-31';
	epoch = epoch.split('-'); epoch[1]--;
	epoch = Date.UTC(... epoch);

	_epoch = epoch;
	return _epoch;
}


/**
 * Constructs a list of downloads required.
 *
 * Based on our companies, we need to determine if they have already been
 * downloaded. We are looking for two ranges:
 *
 * 1. History of earliest date, to first
 */
async function getDownloadParameters(company){
	company = {
		cik: company[1],
		symbol: company[0],
	};

	let today = Today();
	let epoch = Epoch();

	let meta = null;
	try{
		let obj = await global.sec.send(new HeadObjectCommand({Bucket: 'facts', Key: `company/${company.cik}`}));
		meta = obj.Metadata;
		meta = meta.tiingo;
		meta = JSON.parse(meta);
	}
	catch(e){
		meta = null;
	}
	if(!meta){
		meta = {};
		meta = [epoch,epoch];
	}

	company.start = Math.min(...meta);
	company.end = Math.max(...meta);


	// the trick of this is that we have a range of values we received in
	// the past, and need to conver this to a range we want in the future.
	// We can acheive this by taking the old end date, and starting from that
	// point and gathering all dates until today.
	company.start = utils.DayToDate((company.end ?? epoch)+1);
	// if its Saturday or Sunday, move the start foward to Monday.
	while((company.start.getUTCDay()+1) % 7 <= 1){
		company.start.setDate(company.start.getDate()+1);
	}
	company.start = company.start.getTime();
	if(company.start <= today){
		// then there is work to be done
		company.start = (new Date(company.start)).toISOString().split('T').shift();
		company.end = (new Date(today)).toISOString().split('T').shift();
		return company;
	}
	return null;
}


async function DownloadPrices(item) {
	let url = "https://api.tiingo.com/tiingo/daily/{symbol}/prices?startDate={start}&endDate={end}";
	url = url
		.replace(/{symbol}/g,item.symbol)
		.replace(/{start}/g,item.start)
		.replace(/{end}/g,item.end)
		;
	let opts = {
		//'uri': url,
		'headers': {
			'Content-Type': 'application/json',
			'Authorization' : 'Token ' + APIKEY
		}
	};
	//console.log(xmlFile);
	let json = null;
	try{
		let resp = await fetch(url,opts);
		json = await resp.json();
		json.cik = item.cik;
		console.log([item.symbol,item.cik,item.start,item.end,resp.length].join('\t'));
	}
	catch(e){
		debugger;
		console.error(e);
		if(e.stack){
			console.error(e.stack);
		}
		json = null;
	}
	return json;
}



async function BuildFactPackage(body) {
	let json = {
		"type": "company",
		"key": body.cik,
		"data": []
	};
	let recTemplate = {
		cik: body.cik,
		unit: 'usd',
		form: 'prices',
	};
	//body = JSON.parse(body);
	for(let eod of body){

		let date = eod.date.split('T').shift();
		let volume = eod.volume;
		let fy = date.split('-').shift();

		recTemplate.fy = fy;
		recTemplate.date = date;

		if(eod.divCash) eod.dividend = eod.divCash;
		eod.split = eod.splitFactor;

		delete eod.date;
		delete eod.splitFactor;
		delete eod.adjClose;
		delete eod.adjHigh;
		delete eod.adjLow;
		delete eod.adjOpen;
		delete eod.adjVolume;
		delete eod.divCash;

		for(let fact in eod){
			let r = Object.assign({},recTemplate);
			r.filingDate = date;
			r.fact = fact;
			r.val = eod[fact], // (Should multiply this by number of shares)
			json.data.push(r);
		}
	}
	return json;
}



main();
