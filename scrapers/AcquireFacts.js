import * as global from './global.js';

import AcquireFacts from './lib/AcquireFacts.js';
import * as deposit from './lib/deposit.js';

async function main(){
	let factfiller = new AcquireFacts(global.sec);
	let buff = [];
	let stm = factfiller.fetch();
	for await (let company of stm) {
		let receipt = deposit.ReceiveData(company,global.settings)
		let waitRecieptFinish = receipt
			.then(d=>{
				receipt.isResolved = true;
				return d;
			})
			.catch((e)=>{
				receipt.isResolved = true;
				console.error(e.message);
				global.progress.log('ERROR: '+e.message);
				receipt.isResolved = true;
				debugger;
				return null;
			})
			.finally(d=>{
				receipt.isResolved = true;
				waitRecieptFinish.isResolved = true;
			});


		buff.push(waitRecieptFinish);
		// setting the parallelism too high causes a TLS error
		if(buff.length >= 3){
			await Promise.race(buff);
			for(let i = 0; i< buff.length; i++){
				if(buff[i].isResolved){
					buff[i] = buff.at(-1);
					buff.pop();
					i--;
				}
			}
		}

		if(global.halt){
			break;
		}
	}
	await Promise.all(buff);
	global.progress.log('DONE!');
	process.exit();
}

main();
