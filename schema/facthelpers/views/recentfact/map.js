function (doc) {
	if (!(!doc.form || doc.form === 'compdex')) return;
	if (doc.fp === 'day') return;
	var key = doc._id.split('/');
	for(var k in key){
	  key[k] = parseInt(key[k],36);
	}
	var value = key.pop();
	emit(key, value);
  }