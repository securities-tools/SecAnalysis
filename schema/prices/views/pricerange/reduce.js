function (keys, values, rereduce) {
	var value = values.pop();
	while(values.length){
		var item = values.pop();
		value.symbol = value.symbol || item.symbol;
		value.open = value.open||item.open;
		value.close = value.close||item.close;
		if(item.start < value.start){
			value.start = item.start;
			value.open = item.open;
		}
		if(item.end > value.end){
			value.end = item.end;
			value.close = item.close;
		}
		value.high = Math.max(value.high,item.high);
		value.low  = Math.max(value.low,item.low);
		// value.url = "https://api.tiingo.com/tiingo/daily/{ticker}/prices?startDate={start}&endDate={end}"
		//	.replace(/{ticker}/g,value.ticker)
		//	.replace(/{start}/g,value.start)
		//	.replace(/{end}/g,value.end)
		//	;
	}
	return value;
}