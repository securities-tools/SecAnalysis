function (doc) {
	if(doc.fact !== 'close') return;
	if(!doc.symbol) return;
	if(!doc['@']) return;
	
	var key = doc['@'];
	key = new Date(key);
	key = [
		key.getUTCFullYear(),
		key.getUTCMonth()+1,
		doc.symbol,
		doc.cik
	];
	emit(key, 1);
}
