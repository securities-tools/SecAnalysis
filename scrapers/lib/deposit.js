import fs from 'node:fs';
import path from 'node:path';
import {Readable, Stream} from 'node:stream';
import {ReadableStream} from 'node:stream/web';
import zlib from 'zlib';
import s3Stream from 's3-upload-stream';
import { pipeline } from 'node:stream/promises';
import {
	S3Client,
	//ListBucketsCommand,
	//ListObjectsV2Command,
	GetObjectCommand,
	PutObjectCommand,
	DeleteObjectCommand,
	CopyObjectCommand,
	HeadObjectCommand,
} from "@aws-sdk/client-s3";
import { Upload } from "@aws-sdk/lib-storage";
import { progress } from '../global.js';
import * as utils from './utils.js';
import BigMath from './BigMath.js';

const ESTIMATEFLAG = 2n**(BigInt(BigUint64Array.BYTES_PER_ELEMENT)*8n-1n);
const MAX_VALUE =    ESTIMATEFLAG-1n;

import JSONBigint from 'json-bigint';
const JSONBig = JSONBigint({strict:true, useNativeBigInt:true, alwaysParseAsBig: true});

export {
	ReceiveData as default,
	ReceiveData,
	WriteSymbols,
	getS3Client,
};

let S3 = null;
function getS3Client(settings){
	if (S3) return S3;

	let ACCOUNT_ID = settings.storage.r2Facts.AcctId;
	let ACCESS_KEY_ID = settings.storage.r2Facts.AccessKey;
	let SECRET_ACCESS_KEY = settings.storage.r2Facts.SecretKey;

	//console.log(`ACCOUNT_ID.........: ${ACCOUNT_ID} `);
	//console.log(`ACCESS_KEY_ID......: ${ACCESS_KEY_ID} `);
	//console.log(`SECRET_ACCESS_KEY..: ${SECRET_ACCESS_KEY} `);

	S3 = new S3Client({
		region: "auto",
		endpoint: `https://${ACCOUNT_ID}.r2.cloudflarestorage.com`,
		credentials: {
			accessKeyId: ACCESS_KEY_ID,
			secretAccessKey: SECRET_ACCESS_KEY,
		},
	});

	return S3;
}

let _cache = null;
function getCache(){
	if(_cache) return _cache;
	let fPath = new Date().toISOString().replace(/[^0-9]/g,'').substring(0,12);
	fPath = path.join('.', 'cache', `deposit-${fPath}`);
	fs.mkdirSync(fPath,{ recursive: true });
	_cache = fPath;
	return _cache;
}

function hasher(buffer,bits=32){
	if(typeof buffer === 'string'){
		buffer = buffer
			.toLowerCase()
			.split('')
			.map(d=>{return d.charCodeAt(0);})
			;
	}
	if(typeof buffer[Symbol.iterator] !== 'function'){
		console.debug(`buffer: ${buffer}`);
	}
	let size = BigInt(Math.pow(2,bits));
	let hash = 0n;
	for(let d of buffer){
		hash *= 2n;
		hash += BigMath.floor(hash/size);
		hash %= size;
		hash ^= BigInt(d);
	}
	hash = BigMath.abs(hash);
	return hash;
};



async function ParseRequest(context){
	let formData = await context.request.json();
	console.log('Changes requested: ');
	console.log(formData);

	let type = formData.type;
	let key = formData.key;

	formData.key = key;
	delete formData.type;

	return formData;
}



function UpdateDates(updRecs){
	let dates = Object
		.keys(updRecs)
		.map(d=>Number(d))
		.sort((a,b)=>{return b-a;})
		;
	return dates;
}



function PrepareUpdateRecords(updRecs){
	updRecs = updRecs.data;
	updRecs = updRecs.filter((d)=>{
		return ( true
			&& 'date' in d
			&& 'fact' in d
			&& 'unit' in d
			&& 'val'  in d
		);
	});
	//console.log(updRecs);
	updRecs = updRecs.map((d)=>{
		//console.debug(`Prepare: ${JSONBig.stringify(d)}`);

		let date = utils.DateToDay(d.date);

		let val = d.val;
		//console.log(val);
		val = val ?? 0;
		let type = 'number';
		if(typeof val === 'number'){
			val = Math.floor(val*10000);
		}
		else if(typeof val === 'bigint'){
			val *= 10000;
		}
		else{
			//console.log(typeof val);
			let testval = utils.DateToDay(val);
			if(!Number.isNaN(testval)){
				type = 'date';
				val = testval;
			}
			else{
				type = 'string';
				val = hasher(val.toString(), BigUint64Array.BYTES_PER_ELEMENT*8);
			}
		}
		val = BigInt(val);
		//console.log(val);:

		let fld = hasher(d.fact.toLowerCase(), BigUint64Array.BYTES_PER_ELEMENT*8);
		//console.log(fld);

		return { date:date, fld:fld, fact:d.fact, val:val , type: type};
	});
	updRecs = updRecs.reduce((a,d)=>{
		a[d.date] = a[d.date] ?? {};
		a[d.date][d.fld] = d;
		return a;
	},{});
	for(let d in updRecs){
		updRecs[d] = Object.values(updRecs[d]);
	}


	//console.debug('');
	//console.debug('# Prepare Updates');
	//console.debug(`PrepareUpdates > updRecs: ${JSONBig.stringify(updRecs)}`);

	return updRecs;
}


async function PrepareFields(context, updates){
	let isChanged = false;
	let fields = {};
	let triesagain = 3;
	while(--triesagain>0){
		try{
			let text = '';
			fields = await context.send(new GetObjectCommand({Bucket: 'facts', Key: 'views/facts'}));
			for await (let chunk of fields.Body){
				text += chunk.toString('utf-8');
			}
			fields = JSONBig.parse(text);
			triesagain = 0;
		}
		catch(e){
			if(triesagain === 0){
				console.error(e);
			}
			fields = {};
		}
	}
	// integrate the newly sent fields
	for(let u of Object.values(updates)){
		for(let f of u){
			if(!(f.fact in fields) || typeof fields[f.fact] === 'number'){
				fields[f.fact] = {
					id:f.fld,
					type:f.type,
				};
				isChanged = true;
			}
		}
	}
	// if there were new fields, save the object
	fields.isChanged = isChanged ? 1 : 0;
	//console.log(`PrepareFields > fieldsChange: ${isChanged}`);
	if(isChanged){
		// don't stop processing, but do flag that the
		// process must wait for this before terminatining
		let body = JSONBig.stringify(fields);
		context.send(new PutObjectCommand({
			Bucket: 'facts',
			Key: 'views/facts',
			Body: body
		}));
	}
	return fields;
}



async function OpenRemoteMeta(db, company, prog){
	// Get the existing document from the
	let key = `company/${company}`;
	let meta = null;
	//log(company, 'Fetching Meta');
	try{
		let doc = await db.send(new HeadObjectCommand({Bucket: 'facts', Key: key}));
		meta = doc.Metadata;
	}
	catch(e){
		//log(company, 'Not Found');
		meta = {};
	}
	return meta;
}


async function OpenRemoteFile(db, company, prog){
	// Get the existing document from the
	let key = `company/${company}`;
	let stm = null;
	//log(company, 'Fetching Data');
	try{
		let filePath = path.resolve(path.join(cachePath,`${company}.in`));
		let outFile = fs.createWriteStream(`${filePath}.br`,{flags:'w'});

		let doc = await db.send(new GetObjectCommand({Bucket: 'facts', Key: key}));
		await pipeline(doc.Body,outFile);

		let compressProg = 0;
		let compressTotal = fs.statSync(`${filePath}.br`).size;
		outFile = fs.createWriteStream(filePath,{flags:'w'});
		let infile = fs
			.createReadStream(`${filePath}.br`,{flags:'r'})
			.pipe(new Stream.Transform({
				async transform(inboundBuffer, encoding, done) {
					compressProg += inboundBuffer.length * inboundBuffer.BYTES_PER_ELEMENT;
					let pct = compressProg/compressTotal;
					pct *= 0.5;
					pct *= 100;
					pct = Math.floor(pct);
					prog.update(pct,{label:`${company}`});
					this.push(inboundBuffer);
					done();
				}
			}))
			.pipe(zlib.createBrotliDecompress())
			//.pipe(zlib.createGunzip())

			;
		await pipeline(infile,outFile);
		stm = fs.createReadStream(filePath,{flags:'r'});
	}
	catch(e){
		//log(company,'Not Found');
		let v = new BigUint64Array([
			//meta data
			company, 2, MAX_VALUE, 0, 0,
			//header row
			1, 0,
			//body row
			0, 0,
		].map(d=>BigInt(d)));
		v = new Uint8Array(v.buffer);
		stm = Stream.Readable.from([v]);
	}
	return stm;
}


function SanitizeFields(fields){
	if (!Array.isArray(fields)) {
		console.log(fields);
		throw Error('Must be an array of integer fields');
	}
	fields = new Set(fields);
	fields = Array.from(fields);
	fields = fields
		.filter(d=>{
			return (['bigint','number'].includes(typeof d));
		})
		.map(d=>{
			return BigInt(d);
		})
		.filter(d=>{
			return (d!==0n && d!==1n);
		})
		.sort((a,b)=>{
			if(a<b) return -1;
			else if (a>b) return +1;
			return 0;
		})
		;
	fields = [1n].concat(fields,[0n]);
	fields = new BigUint64Array(fields);
	return fields;
}



function *DataSizeReader(chunk,params) {
	params.processor = CreateHeaderField;

	let sizes = Array.from(chunk);

	//console.debug(``);
	//console.debug(`# Meta`);
	//console.debug(`Company.....: ${sizes[0]}`);
	//console.debug(`oldWidth....: ${sizes[1]}`);
	//console.debug(`oldFirstDay.: 0x${sizes[2].toString(16)}`);
	//console.debug(`oldLastDay..: 0x${sizes[3].toString(16)}`);
	//console.debug(``);

	if(sizes.at(-1) !== 0n){
		let msg = 'Invalid company layout. Malformed metadata.';
		console.error(msg); 
		debugger;
		throw new Error(msg);
	}

	params.company = sizes[0];
	params.oldWidth = Number(sizes[1]);
	params.oldFirstDay = sizes[2];
	params.oldLastDay = sizes[3];

	params.nullfld = params.oldWidth-1;
	params.buffRecByteLen = params.oldWidth * BigUint64Array.BYTES_PER_ELEMENT;

	// must return an empty array for the outbound stream
	yield [];
}



function *CreateHeaderField(oldFields,params){
	try{
		params.oldFields = Array.from(oldFields);
		// the next time the processor is called, it
		// should call createbody, not this funciton
		params.processor = CreateBody;

		params.newFields = Array.from(params.newFields).map(d=>d.id);
		params.newFields = params.newFields.concat(params.oldFields);
		params.newFields = SanitizeFields(params.newFields);

		params.newWidth = params.newFields.length;
		params.blankRec = params.newFields.slice(0);
		params.blankRec = params.blankRec.fill(0n|ESTIMATEFLAG);
		params.prevRec = params.blankRec.slice(0);
		params.fieldMap = Array.from(params.blankRec);

		params.newFirstDay = BigMath.min(params.oldFirstDay,params.updDateMin);
		params.newLastDay = BigMath.max(params.oldLastDay,params.updDateMax);

		//console.debug(``);
		//console.debug(`# Header`);
		//console.debug(`- Company...: ${params.company}`);
		//console.debug(`- width.....: ${params.oldWidth} --> ${params.newWidth}`);
		//console.debug(`- Fields....: ${params.oldFields.length} --> ${params.newFields.length}`);
		//console.debug(`- FirstDay..: ${params.oldFirstDay} --> 0x${params.newFirstDay.toString(16)}`);
		//console.debug(`- LastDay...: ${params.oldLastDay} --> 0x${params.newLastDay.toString(16)}`);
		////console.debug(`- oldFields.: ${params.oldFields.slice(0,3)} ... ${params.oldFields.slice(-3)}`);
		////console.debug(`- newFields.: ${params.newFields.slice(0,3)} ... ${params.newFields.slice(-3)}`);
		//console.debug(`- oldFields.: ${params.oldFields}`);
		//console.debug(`- newFields.: ${params.newFields}`);
		//console.debug(``);

		if(params.oldWidth >= Number.MAX_SAFE_INTEGER){
			throw new Error(`Issue with field list in old Dataset. Field width is ${params.oldWidth} which exceed max limits`);
		}
		if(params.oldWidth < 2){
			throw new Error(`Issue with field list in old Dataset. Field width is ${params.oldWidth} which is less than min limits`);
		}
		if(params.oldFields.length != params.oldWidth || params.oldFields.at(-1) !== 0n || params.oldFields.at(0) !== 1n){
			throw new Error('Issue with field list in old Dataset. Retrieved '+params.oldFields.length+' elements and found value '+params.oldFields.at(-1).toString(16));
		}

		params.fieldMap[0] = 0;
		params.fieldMap.isChanged = false;
		for(let i=1, m=1; m<params.newWidth; m++){
			if(params.oldFields[i] === params.newFields[m]){
				params.fieldMap[m] = i;
				i++;
			}
			else{
				params.fieldMap[m] = params.nullfld;
				params.fieldMap.isChanged = true;
			}
		}
		params.newFieldMap = params.newFields.reduce((a,d,i)=>{
			a[d] = i;
			return a;
		},{});

		if(params.newFields.length != params.newWidth || params.newFields.at(-1) !== 0n){
			throw new Error('Issue with field list in new Dataset. Retrieved '+params.newFields.length+' elements and found value '+params.newFields.at(-1).toString(16));
		}

		// encode the new
		let result = [params.company, params.newWidth, params.newFirstDay, params.newLastDay, 0n].map(d=>BigInt(d));
		yield result;
		result = Array.from(params.newFields);
		yield result;
	}
	catch(e){
		console.error(`'CreateHeaderField' general failure`);
		debugger;
		throw e;
	}
}



/**
 * Given the inbound `record`, we apply each `update` that matches the
 * date field (position `0`).
 *
 * The param field contains the previous record. This is used to carry
 * forward any new values which would represent estimates going forward.
 * We apply these by replacing any old estimates with the new value, set
 * as an estimate.
 *
 * @param {*} params
 * @param {*} record
 * @param {*} updates
 * @returns
 */
function ApplyUpdatesToChunk(params,record,updates){
	try{
		let currdate = record[0];
		// for each field in the record
		for(let i in record){
			// check to see if it is an estimated value
			i = Number(i);
			if(record[i] & ESTIMATEFLAG){
				// if it was a projected value, we should overwrite it with the current
				// projection. projections are just ... whatever the most recent value
				// was. We should also make sure we point out this is a projection we are making
				record[i] = params.prevRec[i] | ESTIMATEFLAG;
			}
		}

		//console.debug(`- Apply ${record.length}@${record[0]} (${record.at(-1)}) `);
		// going through our update lists, check to see if our most recent element is associated with the current record's date
		let upd = updates[currdate];
		if(upd){
			// they are the same date and are therefore meant for each other
			for(let update of upd){
				let fld = update.fld;
				update.col = params.newFieldMap[fld];
				record[update.col] = update.val;
				/*
				console.debug(`  - ${JSONBig.stringify(update)} `);
				console.debug(`  - ${fld} `);
				console.debug(`  - ${JSONBig.stringify(params.newFieldMap)} `);
				console.debug(`  - ${update.col} `);
				console.debug(`  - ${record[update.col]} `);
				*/
			}
			delete updates[currdate];
		}
		record[record.length-1] = 0n;
		record[0] = currdate;

		params.prevRec = record.slice();

		return record;
	}
	catch(e){
		console.error(`'ApplyUpdatesToChunk' general failure`);
		throw e;
	}
}



function *CreateBody(oldRec,params){
	try{
		oldRec = oldRec.slice(0);

		// Dates are hard to preserve. We assume that the date should be the 
		// previous record +1, and set this on every record. THere have been 
		// errors where this got missed and it is simpler ot just do it every 
		// time.
		let oldDate = params.prevRec[0]+1n;
		if(oldDate === ESTIMATEFLAG+1n){
			oldDate = params.newFirstDay;
		}
		oldRec[0] = oldDate;

		/*
		This is to handle the case where our update record is earlier than
		our earliest existing record.

		In this case, we don't have an oldRec (yet), so we need to kind of
		just make a blank one. This blank one will have the update applied
		to it.
		*/
		//console.debug(`Transform > New Records Front: ${params.updDateMin} --> ${oldDate} ... ${oldDate-params.updDateMin}`);
		for(let day=params.newFirstDay; oldDate === params.oldFirstDay && day < oldDate; day++ ){
			let newRec = params.blankRec.slice(0);
			newRec[0] = day;
			newRec = ApplyUpdatesToChunk(params,newRec,params.updRecs);
			yield newRec;
		}

		/*
		At this point we have an old record that needs updating. We need to
		make sure it matches the field width of the new format. So if we added
		any new fields, we need to make sure we map them across to the new
		record format and leave a blank field in the space.

		We start by creating a blank record, and then copying the old values
		onto it.
		*/
		let newRec = params.blankRec.slice(0);
		if(params.fieldMap.isChanged){
			for(let i=0; i<newRec.length; i++){
				let mapLoc = params.fieldMap[i];
				newRec[i] = (mapLoc === params.nullfld) ? 0n|ESTIMATEFLAG : oldRec[mapLoc];
			}
		}
		newRec[0] = oldDate;
		newRec = ApplyUpdatesToChunk(params,newRec,params.updRecs);
		yield newRec;

		//console.debug(`Transform > New Records Back.: ${oldDate+1} --> ${params.updDateMax} ... ${params.updDateMin-(oldDate+1)}`);
		for(let day=oldDate+1n; day > params.oldLastDay && day <= params.newLastDay; day++ ){
			let newRec = params.blankRec.slice(0);
			newRec[0] = day;
			newRec = ApplyUpdatesToChunk(params,newRec,params.updRecs);
			yield newRec;
		}
		//console.debug(`Transform > Done.`);
		return [];
	}
	catch(e){
		console.error(`'CreateBody' general failure`);
		debugger;
		throw e;
	}
}



function CalcNewDataset(updFields, updRecs, cik) {
	let dates = UpdateDates(updRecs);
	let params = {
		oldWidth: null,
		oldFirstDay: null,
		oldLastDay: null,
		oldFields: null,

		newWidth: null,
		newFirstDay: null,
		newLastDay: null,
		newFields: Object.values(updFields),

		updDateMax: dates.at(0),
		updDateMin: dates.at(-1),
		nullfld: null,
		updRecs: updRecs,
		updFields: updFields,

		fieldMap: null,
		blankRec: null,
		prevRec: null,

		buffChunk: new Uint8Array(0),
		buffRecByteLen: BigUint64Array.BYTES_PER_ELEMENT*5,

		processor: DataSizeReader,

		total: 0,
		count: 0,
		progress: null,
	};

	//params.progress  = progress.create(params.total, params.count,{label:`${cik}`});
	return new Stream.Transform({
		flush(done){
			progress.remove(params.progress);
			done();
		},
		async transform(inboundBuffer, encoding, done) {
			if (inboundBuffer === null) {
				controller.terminate();
				if(params.buffChunk.length != params.buffPos){
					this.destroy('Data left in buffer at end of processing');
				}
				return;
			}
			let partA = new Uint8Array(params.buffChunk.buffer);
			let partB = new Uint8Array(inboundBuffer.buffer);

			//console.debug(`DataShapeStreamer > inbound...: ${inboundBuffer.length} - ${inboundBuffer.slice(0,3)} ... ${inboundBuffer.slice(-3)}`);
			//console.debug(`DataShapeStreamer > type......: ${inboundBuffer.constructor.name}`);
			let newBuff = new Uint8Array(partA.length+partB.length);
			newBuff.set(partA);
			newBuff.set(partB, partA.length);

			//console.debug(`DataShapeStreamer > newBuff...: ${new BigUint64Array(newBuff.buffer)}`);

			let buffPos = 0;
			while(newBuff.length-buffPos >= params.buffRecByteLen){
				let chunk = new BigUint64Array(newBuff.buffer, buffPos, params.buffRecByteLen/BigUint64Array.BYTES_PER_ELEMENT);
				buffPos += params.buffRecByteLen;

				//console.debug(`DataShapeStreamer > ChunkSize...: ${newBuff.length}`);
				//console.debug(`DataShapeStreamer > BytePos.....: ${buffPos}`);
				//console.debug(`DataShapeStreamer > RecordBytes.: ${params.buffRecByteLen}`);
				//console.debug(`DataShapeStreamer > Counter.....: ${counter++}`);
				//console.debug(Array.from(chunk).map(d=>d.toString(16)));
				// each processor knows when to change to the next processor
				// and handles that internally
				try{
					for await(let result of params.processor(chunk, params)){
						result = new BigUint64Array(result);
						if(result.length === 0){
							continue;
						}

						//params.total = params.newLastDay - params.newFirstDay + 3;
						//params.progress.setTotal(params.total);
						//params.progress.increment();
						params.count++;
						//console.debug(`${params.count} of ${params.total} records`);

						//console.debug(Array.from(result).map(d=>d.toString(16)));
						result = new Uint8Array(result.buffer);
						this.push(result);

					}
					//console.debug('');
				}
				catch(e){
					debugger;
					console.log(e.message)
					this.destroy(e);
				}
			}
			params.buffChunk = newBuff.slice(buffPos)
			done();
			return;
		}

	});
}


function ResolveDelta(source, meta, updates, fields){
	if (!source) return updates;

	source = source.toLowerCase();
	let dates = UpdateDates(updates);

	// random 1 in 10K chance that we just do it anyway
	let force = Math.random();
	force *= 10000;
	force = Math.floor(force);
	force = !force;
	// there was an error state for a while, if it is found, reprocess
	force = force || meta[source]==='[9007199254740991,-9007199254740991]';
	// force the reprocess
	if(force){
		delete meta[source];
	}


	// check the metadata for the previous date bounds
	// the idea here is that we are going to see if this is data we have already processed. If we have, we can just skip it (so delete it).
	// there is an exception that if the field layout has changed, we should process the whole thing again ... just in case.
	let m = meta[source];
	if(m){
		m = JSONBig.parse(m);
		m = m.sort((a,b)=>{if(BigInt(a)<BigInt(b)) return +1; else if (BigInt(a)>BigInt(b)) return -1; return 0;});
		if(!fields.isChanged){
			for(let d of dates){
				if(d <= m[0] && d >= m[1]){
					delete updates[d];
				}
			}
			dates = UpdateDates(updates);
		}
	}

	if(dates.length > 0){
		m = m ?? [Number.MIN_SAFE_INTEGER, Number.MAX_SAFE_INTEGER];
		m[0] = BigMath.max(m[0] ?? Number.MIN_SAFE_INTEGER, dates.at( 0));
		m[1] = BigMath.min(m[1] ?? Number.MAX_SAFE_INTEGER, dates.at(-1));
		meta[source] = JSONBig.stringify(m);
	}

	return updates;
}


fs.mkdirSync(path.resolve(path.join('.', 'cache')),{recursive: true});
let cachePath = fs.mkdtempSync(path.resolve(path.join('.', 'cache', 'deposits-')),{ recursive: true });
function CleanCache(key){
	let paths = fs.readdirSync(cachePath);
	for(let p of paths){
		let d = p.split('.').shift();
		if(`${d}` === `${key}`){
			fs.rmSync(path.join(cachePath,p),{force:true});
		}
	}
}


function log(key, msg){
	key = `${key}`.padStart(10,'.');
	msg = `${key}: ${msg}\n`;
	progress.log(msg);
	return msg;
}


async function IntegrateChanges(context, req){

	if(req.data.length === 0){
		// no data? that was easy
		log(req.key, 'Skipped');
		return {};
	}

	let filePath = path.resolve(path.join(cachePath,`${req.key}`));

	let prog = progress.create(100, 0);
	try{
		prog.update(0,{label:`${req.key}`});


		let updates = await PrepareUpdateRecords(req);
		let fields = await PrepareFields(context, updates);

		//console.log(req.key);
		let meta = await OpenRemoteMeta(context, req.key, prog);
		prog.update(50,{label:`${req.key}`});

		updates = ResolveDelta(req.source, meta, updates, fields);
		if(Object.keys(updates).length === 0){
			// no data? that was easy

			// remove the progress
			prog.stop();
			progress.remove(prog);
			log(req.key, 'Nothing New');
			return {};
		}

		let stm = await OpenRemoteFile(context, req.key, prog);
		let isBad = false;
		let compressProg = 0;
		let compressTotal = 1;
		let inPath = path.resolve(`${filePath}.in`);
		if(fs.existsSync(inPath)){
			compressTotal = fs.statSync(inPath).size;
		}
		stm = stm
			.pipe(CalcNewDataset(fields, updates, req.key))
			.on('error',(e)=>{
				//debugger;
				console.warn(`${req.key}: ${e.message}`);
				isBad = true;
				context
					.send(new CopyObjectCommand({
						Bucket: 'facts',
						Key: `error/${req.key}`,
						CopySource: `facts/company/${req.key}`,
					}))
					.then(d=>{
						console.warn(`${req.key}: Copied to error folder`);
						setTimeout(()=>{
							context
								.send(new DeleteObjectCommand({
									Bucket: 'facts',
									Key: `company/${req.key}`,
								}))
								.then(d=>{
									console.warn(`${req.key}: Removed bad file`);
								});

						},100);
					});
				upload.abort();
			})
			.pipe(new Stream.Transform({
				async transform(inboundBuffer, encoding, done) {
					compressProg += inboundBuffer.length * inboundBuffer.BYTES_PER_ELEMENT;
					compressProg = Math.min(compressProg, compressTotal);
					let pct = compressProg/compressTotal;
					pct *= 0.5;
					pct += 0.5;
					pct *= 100;
					pct = Math.floor(pct);
					prog.update(pct,{label:`${req.key}`});
					this.push(inboundBuffer);
					done();
				}
			}))
			.pipe(zlib.createBrotliCompress())
			//.pipe(zlib.createGzip({level:9}))
			;

		let outFile = path.resolve(`${filePath}.out`);

		const writableStream = fs.createWriteStream(outFile,{flags:'w'});
		await pipeline(stm,writableStream);
		prog.update(67,{label:`${req.key}`});

		let upload;
		let timer = (e = 'upload timed out')=>{
			console.warn(`${req.key}: ${e}`);
			upload.abort();
		}
		let timeout = setTimeout(timer,120000);

		let rtn = null;
		if(isBad){
			prog.update(1,{label:`${req.key}`});
			rtn = new Promise((r)=>{
				setTimeout(r);
			});
		}
		else{
			stm = fs.createReadStream(outFile,{flags:'r'});
			upload = new Upload({
				client: context,
				params: {
					Bucket: 'facts',
					Key: `company/${req.key}`,
					Metadata: meta,
					Body: stm,
					ContentEncoding: 'br',
					ContentType: 'application/vius-braindata',
				},
			});

			upload.on('httpUploadProgress',(d)=>{
				let pct = d.loaded/d.total;
				pct /= 10;
				pct += 9/10;
				pct *= 100;
				pct = Math.round(pct);
				//console.log(`${d.Key}: ${pct}%\n`);
				prog.update(pct,{label:`${req.key}`});
				clearTimeout(timeout);
				timeout = setTimeout(timer,120000);
			});
			rtn = upload.done();
		}

		rtn = await rtn;
		clearTimeout(timeout);
		log(req.key, 'Complete');

		return rtn;
	}
	catch(e){
		rtn = null;
		timer(e.message);
		log(req.key, e.message);
	}
	finally{
		prog.stop();
		progress.remove(prog);
	}

}


async function ReceiveData(company,settings){
	let S3 = getS3Client(settings);
	let rtn = await IntegrateChanges(S3,company);
	CleanCache(company.key);
	return rtn;
}


let WriteSymbolsCounter = 0;
/**
 * Write a given list of symbols to the datastore
 * 
 * @param {*} newSymbols 
 * @returns 
 */
async function WriteSymbols(newSymbols, force=false){
	// we only do this periodically because it is an expensive operation
	if(!force && WriteSymbolsCounter-- > 0) return;
	WriteSymbolsCounter = 135;

	let S3 = getS3Client();

	// get the current symbol list from the database
	let symbols = {};
	let attempts = 3;
	while (attempts-- > 0){
		try{
			let text = '';
			symbols = await S3.send(new GetObjectCommand({Bucket: 'facts', Key: 'views/symbols'}));
			for await (let chunk of symbols.Body){
				text += chunk.toString('utf-8');
			}
			symbols = JSONBig.parse(text);
			attempts = 0;
		}
		catch(e){
			await utils.wait(50*Math.random()+100);
			symbols = {};
		}
	}
	// if we need to create a new object, we can use the line below, but this should be a one time manual event
	if(Object.entries(symbols).length <= 0){
		console.log(`Write Symbols: failed to download existing symbols`);
		return;
	}

	// merge the two lists
	for(let [k,v] of Object.entries(newSymbols)){
		symbols[k] = v;
	}

	// save the merged list to the data store
	console.log(`Write Symbols: ${Object.entries(symbols).length}`);
	symbols = JSONBig.stringify(symbols);
	let rtn = S3.send(new PutObjectCommand({
		Bucket: 'facts',
		Key: 'views/symbols',
		Body: symbols,
		ContentType: 'text/json',
	}));
	return rtn;
}
