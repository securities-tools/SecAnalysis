import stringify from 'json-stable-stringify';
import hasher from 'hash-index';

const ONEDAY = 24 * 60 * 60 * 1000;


export {
	stringify,
	UniqueRec,
	isRecUnique,
	wait,
	utcParse,
	sampler,
	DateToDay,
	DayToDate,
};

let UniqueRec = function UniqueRec(a,b){
	let isSame = b;
	a = a || {};
	b = b || {};
	a = JSON.parse(JSON.stringify(a));
	b = JSON.parse(JSON.stringify(b));

	function remover(marker, obj){
		for(let key of Object.keys(obj)){
			if(key.startsWith(marker)){
				delete obj[key];
			}
		}
	}
	remover('_',a);
	remover('_',b);
	remover('@',a);
	remover('@',b);

	a = stringify(a);
	b = stringify(b);

	if(a === b){
		isSame = null;
	}
	return isSame;
};


let isRecUnique = function isUniqueRec(a,b){
	let isSame = UniqueRec(a,b);
	isSame = (isSame !== null);
	return isSame;
};


let wait = function wait(time=0){
	time = Math.max(time,16);
	return new Promise((resolve)=>{
		setTimeout(resolve,time);
	});
};


let utcParse = function utcParse(date){
	date = date.replace(/[^0-9]/g,'-');
	date = date.split('-');
	date = date.map(d=>{return +d;});
	date[1] = date[1] - 1;
	date = Date.UTC(... date);
	date = new Date(date);
	return date;
};

let sampler = function sampler(value,probability=1.1){
	let dispose = hasher(value,Number.MAX_SAFE_INTEGER) / Number.MAX_SAFE_INTEGER;
	let iskeeper = (dispose < probability);
	return iskeeper;
}


function DateToDay(date){
	if (typeof date === 'bigint'){
		date = Number(date);
	}
	if (typeof date === 'number'){
		date = new Date(date);
	}
	if(date instanceof Date){
		date = date.toISOString();
	}
	date = Math.round(Date.UTC(... date.split('-')) / ONEDAY);
	return date;
}



function DayToDate(date){
	date = new Date(date * ONEDAY);
	return date;
}

